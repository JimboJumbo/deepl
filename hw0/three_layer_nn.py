__author__ = 'tan_nguyen'
import numpy as np
from sklearn import datasets, linear_model
import matplotlib.pyplot as plt

def generate_data():
    '''
    generate data
    :return: X: input data, y: given labels
    '''
    np.random.seed(0)
    X, y = datasets.make_moons(200, noise=0.20)
    return X, y

def plot_decision_boundary(pred_func, X, y):
    '''
    plot the decision boundary
    :param pred_func: function used to predict the label
    :param X: input data
    :param y: given labels
    :return:
    '''
        # Set min and max values and give it some padding
    x_min, x_max = X[0,:].min() - .5, X[0,:].max() + .5
    y_min, y_max = X[1,:].min() - .5, X[1,:].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    X_test = np.c_[xx.ravel(), yy.ravel()]
    Z = pred_func(X_test.T)
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(X[0,:], X[1,:], c=y[1,:], cmap=plt.cm.Spectral)
    plt.show()

########################################################################################################################
########################################################################################################################
# YOUR ASSSIGMENT STARTS HERE
# FOLLOW THE INSTRUCTION BELOW TO BUILD AND TRAIN A 3-LAYER NEURAL NETWORK
########################################################################################################################
########################################################################################################################
class NeuralNetwork(object):
    """
    This class builds and trains a neural network
    """
    def __init__(self, nn_input_dim, nn_hidden_dim , nn_output_dim, actFun_type='sigmoid', reg_lambda=0.01, seed=1):
        '''
        :param nn_input_dim: input dimension
        :param nn_hidden_dim: the number of hidden units
        :param nn_output_dim: output dimension
        :param actFun_type: type of activation function. 3 options: 'tanh', 'sigmoid', 'relu'
        :param reg_lambda: regularization coefficient
        :param seed: random seed
        '''
        self.nn_input_dim = nn_input_dim
        self.nn_hidden_dim = nn_hidden_dim
        self.nn_output_dim = nn_output_dim
        self.actFun_type = actFun_type
        self.reg_lambda = reg_lambda
        
        # initialize the weights and biases in the network
        np.random.seed(seed)
        self.W1 = np.random.randn(self.nn_hidden_dim,self.nn_input_dim) / np.sqrt(self.nn_hidden_dim)
        self.b1 = np.zeros((self.nn_hidden_dim,1))
        self.W2 = np.random.randn(self.nn_output_dim,self.nn_hidden_dim) / np.sqrt(self.nn_output_dim)
        self.b2 = np.zeros((self.nn_output_dim,1))

    def feedforward(self, X):
        '''
        feedforward builds a 3-layer neural network and computes the two probabilities,
        one for class 0 and one for class 1
        :param X: input data
        :param actFun: activation function
        :return:
        '''

        # YOU IMPLEMENT YOUR feedforward HERE

        self.z1 = np.dot(self.W1,X)+self.b1
        self.a1 =actFun(self.z1, self.actFun_type)
        self.z2 =np.dot(self.W2,self.a1)+self.b2
        exp_scores = np.exp(self.z2)
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        return None

    def calculate_loss(self, X, y):
        '''
        calculate_loss computes the loss for prediction
        :param X: input data
        :param y: given labels
        :return: the loss for prediction
        '''
        num_examples = len(X.T)
        self.feedforward(X)#, lambda x: self.actFun(x, type=self.actFun_type))
        # Calculating the loss

        # YOU IMPLEMENT YOUR CALCULATION OF THE LOSS HERE

        data_loss = -np.mean(np.sum(y*np.log(self.probs),axis =1))

        # Add regulatization term to loss (optional)
        # data_loss += self.reg_lambda / 2 * (np.sum(np.square(self.W1)) + np.sum(np.square(self.W2)))
        return (1. / num_examples) * data_loss

    def predict(self, X):
        '''
        predict infers the label of a given data point X
        :param X: input data
        :return: label inferred
        '''
        self.feedforward(X)#, lambda x: self.actFun(x, type=self.actFun_type))
        return np.argmax(self.probs, axis=0)

    def backprop(self, X, y):
        '''
        backprop implements backpropagation to compute the gradients used to update the parameters in the backward step
        :param X: input data
        :param y: given labels
        :return: dL/dW1, dL/b1, dL/dW2, dL/db2
        '''

        # IMPLEMENT YOUR BACKPROP HERE
#        num_examples = len(X.T)
        delta3 = self.probs
#        delta3[y[1,:],range(num_examples)] -= 1
        delta3-=y
#        self.delta2 = self.calculate_loss(X,y)*diff_actFun(self.probs,type=self.actFun_type)
        self.dW2 = np.dot(delta3,self.a1.T)
        self.db2 = np.mean(delta3,axis=1,keepdims=True)
        self.delta1 = np.dot(self.W2.T,delta3)*diff_actFun(self.a1,self.actFun_type)
        self.dW1 = np.dot(self.delta1,X.T)
        self.db1 = np.mean(self.delta1,axis=1,keepdims=True)

        # dW2 = dL/dW2
        # db2 = dL/db2
        # dW1 = dL/dW1
        # db1 = dL/db1
        return self.dW1, self.dW2, self.db1, self.db2

    def fit_model(self, X, y, epsilon=0.001, num_passes=1000, print_loss=True):
        '''
        fit_model uses backpropagation to train the network
        :param X: input data
        :param y: given labels
        :param num_passes: the number of times that the algorithm runs through the whole dataset
        :param print_loss: print the loss or not
        :return:
        '''
        # Gradient descent.
        for i in range(0, num_passes):
            # Forward propagation
            self.feedforward(X)
            # Backpropagation
            self.dW1, self.dW2, self.db1, self.db2 = self.backprop(X, y)

            # Add regularization terms (b1 and b2 don't have regularization terms)
            self.dW2 += self.reg_lambda * self.W2
            self.dW1 += self.reg_lambda * self.W1

            # Gradient descent parameter update
            self.W1 += -epsilon * self.dW1
            self.b1 += -epsilon * self.db1
            self.W2 += -epsilon * self.dW2
            self.b2 += -epsilon * self.db2

            # Optionally print the loss.
            # This is expensive because it uses the whole dataset, so we don't want to do it too often.
            if print_loss and i % 1000 == 0:
                print("Loss after iteration %i: %f" % (i, self.calculate_loss(X, y)))

    def visualize_decision_boundary(self, X, y):
        '''
        visualize_decision_boundary plots the decision boundary created by the trained networknew
        :param X: input data
        :param y: given labels
        :return:
        '''
        plot_decision_boundary(lambda x: self.predict(x), X, y)
        
def actFun(z, type):
        '''
        actFun computes the activation functions
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: activations
        '''
        # YOU IMPLMENT YOUR actFun HERE
        if type=='sigmoid':
            return 1.0 / (1.0 + np.exp(-z))
        elif type == 'Relu':
            return (np.abs(z)+z)/2.0
        elif type == 'tanh':
            return np.tanh(z)

        # return None

def diff_actFun(z, type):
    '''
    diff_actFun computes the derivatives of the activation functions wrt the net input
    :param z: net input
    :param type: Tanh, Sigmoid, or ReLU
    :return: the derivatives of the activation functions wrt the net input
    '''

    # YOU IMPLEMENT YOUR diff_actFun HERE
    if type == 'sigmoid':
        return actFun(z,type) * (1 - actFun(z,type))
    elif type == 'tanh':
        return 1.0 - np.tanh(z)**2
    elif type == 'Relu':
        return 1-np.uint8(actFun(z,type = 'Relu')==0)
    # return None

#def main():
    # generate and visualize Make-Moons dataset
X, y = generate_data()
plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)
plt.show()
X_input = X.T
y_output = np.array([1-y,y])
model = NeuralNetwork(nn_input_dim=2, nn_hidden_dim=3, nn_output_dim=2, actFun_type='tanh',reg_lambda=0.01)
model.fit_model(X_input,y_output)
y_valid = model.predict(X_input)
print(np.sum(np.abs(y-y_valid))/200.0)
model.visualize_decision_boundary(X_input,y_output)

#if __name__ == "__main__":
#    main()