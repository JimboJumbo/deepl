#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#import theano
#theano.config.device='gpu-011'
#theano.config.floatX = 'float32'
#import theano.sandbox.cuda
#theano.sandbox.cuda.use('gpu11')
import os
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import Adam
from keras.utils import np_utils
from sklearn.cross_validation import train_test_split as tts
import keras.callbacks as callbacks
import numpy as np
from pandas.io.parsers import read_csv
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import csv

FTRAIN = '~/training.csv'
FTEST = '~/test.csv'


#df = read_csv(os.path.expanduser(FTRAIN)) 
#colname = [i for i in df.columns if i!='Image']
#sub = []
#with open('IdLookupTable.csv', 'rb') as csvfile:
#    spamreader = csv.reader(csvfile, dialect='excel')
#    for row in spamreader:
#        sub.append(row)
#sub.pop(0)
#sub = np.array(sub)
#ID = sub[:,1].astype(np.uint16)
#point = sub[:,2]
#indexnum = np.zeros(len(point))
#indexnum[0] = colname.index(point[0])
#count = 0
#for i in range(1,len(ID)):
#    if ID[i] != ID[i-1]:
#        count+=30
#    indexnum[i] = count+colname.index(point[i])
#
#indexnum = indexnum.astype(np.uint16)
#np.save('indexnum.npy',indexnum)
indexnum = np.load('indexnum.npy')
#%%        
def load(test=False, cols=None):

    fname = FTEST if test else FTRAIN
    df = read_csv(os.path.expanduser(fname))  # load pandas dataframe

    # The Image column has pixel values separated by space; convert
    # the values to numpy arrays:
    df['Image'] = df['Image'].apply(lambda im: np.fromstring(im, sep=' '))

    if cols:  # get a subset of columns
        df = df[list(cols) + ['Image']]

#    print(df.count())  # prints the number of values for each column
    df = df.dropna()  # drop all rows that have missing values in them

    X = np.vstack(df['Image'].values) / 255.  # scale pixel values to [0, 1]
    X = X.astype(np.float32)
    X = np.reshape(X,(np.size(X,0),1,96,96))

    if not test:  # only FTRAIN has any target columns
        y = df[df.columns[:-1]].values
        y = (y - 48) / 48  # scale target coordinates to [-1, 1]
        X, y = shuffle(X, y, random_state=42)  # shuffle train data
        y = y.astype(np.float32)
    else:
        y = None

    return X, y


X, y = load()
test_data,nu = load(test=True)
#plt.imshow(X[0,0,:,:])

#%%

X_train,X_val_test,y_train, y_val_test = tts(X,y,test_size=0.3,random_state=33)
X_val,X_test,y_val, y_test = tts(X_val_test,y_val_test,test_size=0.5,random_state=33)

X_train = X_train.astype('float32')
X_val = X_val.astype('float32')
X_test = X_test.astype('float32')

#%%
acti = 'relu'
epoch = 100
model = Sequential()
# Two CONV layer with maxpooling and dropout
model.add(Convolution2D(32, 3, 3, border_mode='valid', input_shape=(1,96,96)))
model.add(Activation(acti))
model.add(Convolution2D(32, 3, 3))
model.add(Activation(acti))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
# Two CONV layer with maxpooling and dropout
model.add(Convolution2D(64, 3, 3, border_mode='valid'))
model.add(Activation(acti))
model.add(Convolution2D(64, 3, 3))
model.add(Activation(acti))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
#Two fully connect layer with dropout
model.add(Dense(256))
model.add(Activation(acti))
model.add(Dropout(0.5))

model.add(Dense(30))
model.add(Activation('tanh'))

model.compile(loss='mse', optimizer=Adam())
earlystop = callbacks.EarlyStopping(monitor = 'val_loss', patience=6)
model.fit(X_train, y_train, batch_size=32, nb_epoch=epoch,validation_data=(X_val,y_val),callbacks=[earlystop])

#Predict result
pred = model.predict(test_data,batch_size=32)
np.save('pred.npy',pred)
#a = np.sqrt(pred - y_test)

prediction = pred.flatten()
prediction = prediction[indexnum]
prediction = (prediction*48)+48
with open('Submission.csv', 'wb') as csvfile:
    csvwriter = csv.writer(csvfile, dialect='excel')
    csvwriter.writerow(['RowId','Location'])
    count = 1
    for i in prediction:        
        csvwriter.writerow([count,i])
        count+=1
