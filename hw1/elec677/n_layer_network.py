__author__ = 'tan_nguyen'
import numpy as np
from sklearn import datasets, linear_model
import matplotlib.pyplot as plt

def generate_data():
    '''
    generate data
    :return: X: input data, y: given labels
    '''
    np.random.seed(0)
#    X, y = datasets.make_moons(200, noise=0.20)
    X, y = datasets.make_circles(200, noise=0.1,factor = 0.4)
    return X, y

def plot_decision_boundary(pred_func, X, y):
    '''
    plot the decision boundary
    :param pred_func: function used to predict the label
    :param X: input data
    :param y: given labels
    :return:
    '''
    # Set min and max values and give it some padding
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.show()

########################################################################################################################
########################################################################################################################
# YOUR ASSSIGMENT STARTS HERE
# FOLLOW THE INSTRUCTION BELOW TO BUILD AND TRAIN A 3-LAYER NEURAL NETWORK
########################################################################################################################
########################################################################################################################
class NeuralNetwork(object):
    """
    This class builds and trains a neural network
    """
    def __init__(self, sizes, actFun_type='sigmoid', reg_lambda=0.01, seed=0):
        '''
        :param nn_input_dim: input dimension
        :param nn_hidden_dim: the number of hidden units
        :param nn_output_dim: output dimension
        :param actFun_type: type of activation function. 3 options: 'tanh', 'sigmoid', 'relu'
        :param reg_lambda: regularization coefficient
        :param seed: random seed
        '''
        self.sizes = sizes
        self.numOfLayers = len(sizes)
        self.actFun_type = actFun_type
        self.reg_lambda = reg_lambda
        self.W = []
        self.b = []
        # initialize the weights and biases in the network
        np.random.seed(seed)
        for i in range(self.numOfLayers-1):
            self.W.append(np.random.randn(self.sizes[i],self.sizes[i+1]) / np.sqrt(self.sizes[i]))
            self.b.append(np.zeros((1,self.sizes[i+1])))
               
        self.dW = [np.zeros(w.shape) for w in self.W]
        self.db = [np.zeros(b.shape) for b in self.b]

    def feedforward(self, X, actFun):
        '''
        feedforward builds a 3-layer neural network and computes the two probabilities,
        one for class 0 and one for class 1
        :param X: input data
        :param actFun: activation function
        :return:
        '''

        # YOU IMPLEMENT YOUR feedforward HERE
        self.z = []
        self.a = []
        self.a.append(X)
        self.z.append(X.dot(self.W[0])+self.b[0])
        # YOU IMPLEMENT YOUR feedforward HERE
        for i in range(1,self.numOfLayers-1):
            self.a.append(actFun(self.z[-1]))
            self.z.append(self.a[-1].dot(self.W[i])+self.b[i])
            
        exp_scores = np.exp(self.z[-1])
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        return None

    def calculate_loss(self, X, y):
        '''
        calculate_loss computes the loss for prediction
        :param X: input data
        :param y: given labels
        :return: the loss for prediction
        '''
        num_examples = len(X)
        self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
        # Calculating the loss

        # YOU IMPLEMENT YOUR CALCULATION OF THE LOSS HERE

        # data_loss =
        corect_logprobs = -np.log(self.probs[range(num_examples), y])
        data_loss = np.sum(corect_logprobs)
        # Add regulatization term to loss (optional)
#        data_loss += self.reg_lambda / 2 * (np.sum(np.square(self.W1)) + np.sum(np.square(self.W2)))
        return (1. / num_examples) * data_loss

    def predict(self, X):
        '''
        predict infers the label of a given data point X
        :param X: input data
        :return: label inferred
        '''
        self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
        return np.argmax(self.probs, axis=1)

    def backprop(self, X, y):
        '''
        backprop implements backpropagation to compute the gradients used to update the parameters in the backward step
        :param X: input data
        :param y: given labels
        :return: dL/dW1, dL/b1, dL/dW2, dL/db2
        '''
        # IMPLEMENT YOUR BACKPROP HERE
        num_examples = len(X)
        
        delta = self.probs
        delta[range(num_examples), y] -= 1
        self.dW[-1] = (self.a[-1].T).dot(delta)
        self.db[-1] = np.sum(delta, axis=0, keepdims=True) 

        for i in xrange(1,self.numOfLayers-1):
            delta = delta.dot(self.W[-1-i+1].T)*self.diff_actFun(self.z[-1-i],self.actFun_type)
            self.dW[-1-i] = np.dot(self.a[-1-i].T,delta)
            self.db[-1-i] = np.sum(delta,axis=0,keepdims=True)
            
        return self.dW, self.db

    def fit_model(self, X, y, epsilon=0.01, num_passes=2000, print_loss=True):
        '''
        fit_model uses backpropagation to train the network
        :param X: input data
        :param y: given labels
        :param num_passes: the number of times that the algorithm runs through the whole dataset
        :param print_loss: print the loss or not
        :return:
        '''
        # Gradient descent.
        for i in range(0, num_passes):
            # Forward propagation
            self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
            # Backpropagation
            self.dW, self.db = self.backprop(X, y)

            # Add regularization terms (b1 and b2 don't have regularization terms)
            self.dW = [delta_w + self.reg_lambda*w for delta_w,w in zip(self.dW,self.W)]

            # Gradient descent parameter update
            self.W = [ww - epsilon*delta_ww for ww,delta_ww in zip(self.W,self.dW)]
            self.b = [bb - epsilon*delta_bb for bb,delta_bb in zip(self.b,self.db)]

            # Optionally print the loss.
            # This is expensive because it uses the whole dataset, so we don't want to do it too often.
            if print_loss and i % 1000 == 0:
                print("Loss after iteration %i: %f" % (i, self.calculate_loss(X, y)))

    def visualize_decision_boundary(self, X, y):
        '''
        visualize_decision_boundary plots the decision boundary created by the trained network
        :param X: input data
        :param y: given labels
        :return:
        '''
        plot_decision_boundary(lambda x: self.predict(x), X, y)
        
        
    def actFun(self, z, type):
            '''
            actFun computes the activation functions
            :param z: net input
            :param type: Tanh, Sigmoid, or ReLU
            :return: activations
            '''
            # YOU IMPLMENT YOUR actFun HERE
            if type=='sigmoid':
                return 1.0 / (1.0 + np.exp(-z))
            elif type == 'Relu':
                return (np.abs(z)+z)/2.0
            elif type == 'tanh':
                return np.tanh(z)
    
            # return None
    
    def diff_actFun(self, z, type):
        '''
        diff_actFun computes the derivatives of the activation functions wrt the net input
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: the derivatives of the activation functions wrt the net input
        '''
    
        # YOU IMPLEMENT YOUR diff_actFun HERE
        if type == 'sigmoid':
            return self.actFun(z,type) * (1 - self.actFun(z,type))
        elif type == 'tanh':
            return 1.0 - np.tanh(z)**2
        elif type == 'Relu':
            return 1-np.uint8(self.actFun(z,type = 'Relu')==0)
            
            
def main():
    # # generate and visualize Make-Moons dataset
    X, y = generate_data()
    plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)
    plt.show()
    
    model = NeuralNetwork([2,5,5,2], actFun_type='sigmoid',reg_lambda=0.01)
    model.fit_model(X,y)
    model.visualize_decision_boundary(X,y)

if __name__ == "__main__":
    main()