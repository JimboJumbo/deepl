from scipy import misc
import numpy as np
import tensorflow as tf
import random
import matplotlib.pyplot as plt
import matplotlib as mp

# --------------------------------------------------
# setup

def weight_variable(shape,name):
    '''
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    '''

    # IMPLEMENT YOUR WEIGHT_VARIABLE HERE
#    initial = tf.truncated_normal(shape, stddev=0.1)
#    W = tf.Variable(initial, name = name)
    W = tf.get_variable(name, shape = shape, initializer=tf.contrib.layers.xavier_initializer_conv2d())
    return W

def bias_variable(shape):
    '''
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    '''

    # IMPLEMENT YOUR BIAS_VARIABLE HERE
    initial = tf.constant(0.1, shape=shape) 
    b = tf.Variable(initial)
#    b = tf.get_variable("b", shape = shape, initializer=tf.contrib.layers.xavier_initializer())
    return b

def conv2d(x, W):
    '''
    Perform 2-D convolution
    :param x: input tensor of size [N, W, H, Cin] where
    N: the number of images
    W: width of images
    H: height of images
    Cin: the number of channels of images
    :param W: weight tensor [w, h, Cin, Cout]
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters = the number of channels of images
    Cout: the number of filters
    :return: a tensor of features extracted by the filters, a.k.a. the results after convolution
    '''

    # IMPLEMENT YOUR CONV2D HERE
    h_conv = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')
    return h_conv

def max_pool_2x2(x):
    '''
    Perform non-overlapping 2-D maxpooling on 2x2 regions in the input data
    :param x: input data
    :return: the results of maxpooling (max-marginalized + downsampling)
    '''

    # IMPLEMENT YOUR MAX_POOL_2X2 HERE
    h_max = tf.nn.max_pool(x, ksize=[1, 2, 2, 1],strides=[1, 2, 2, 1], padding='SAME')
    return h_max

def act(var):
#    activation = tf.nn.sigmoid(var)
    activation = tf.nn.relu(var)
#    activation  = tf.nn.tanh(var)
    return activation

ntrain = 1000 # per class
ntest =  100# per class
nclass =  10# number of classes
imsize = 28
nchannels = 1
batchsize = 32

Train = np.zeros((ntrain*nclass,imsize,imsize,nchannels))
Test = np.zeros((ntest*nclass,imsize,imsize,nchannels))
LTrain = np.zeros((ntrain*nclass,nclass))
LTest = np.zeros((ntest*nclass,nclass))

itrain = -1
itest = -1
filepath = '/home/osboxes/CloudStation/ELEC677/hw2'
for iclass in range(0, nclass):
    for isample in range(0, ntrain):
        path = '/home/osboxes/CloudStation/ELEC677/hw2/CIFAR10/Train/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itrain += 1
        Train[itrain,:,:,0] = im
        LTrain[itrain,iclass] = 1 # 1-hot lable
    for isample in range(0, ntest):
        path = '/home/osboxes/CloudStation/ELEC677/hw2/CIFAR10/Test/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itest += 1
        Test[itest,:,:,0] = im
        LTest[itest,iclass] = 1 # 1-hot lable

sess = tf.InteractiveSession()

tf_data = tf.reshape(tf.placeholder(tf.float32, shape=[None, 784]), [-1,28, 28,1])#tf variable for the data, remember shape is [None, width, height, numberOfChannels] 
tf_labels = tf.placeholder(tf.float32, shape=[None, 10])#tf variable for labels

# --------------------------------------------------
# model
#create your model
W_conv1 = weight_variable([5, 5, 1, 32],"conv1")
b_conv1 = bias_variable([32])
h_conv1 = act(conv2d(tf_data, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1)

# second convolutional layer
W_conv2 = weight_variable([5, 5, 32, 64],"conv2")
b_conv2 = bias_variable([64])
h_conv2 = act(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2)

# densely connected layer
W_fc1 = weight_variable([7 * 7 * 64, 1024],"fc1")
b_fc1 = bias_variable([1024])
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = act(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# dropout
keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# softmax
W_fc2 = weight_variable([1024, 10],"fc2")
b_fc2 = bias_variable([10])    
y_conv=tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)
# --------------------------------------------------
# loss
#set up the loss, optimization, evaluation, and accuracy
cross_entropy = tf.reduce_mean(-tf.reduce_sum(tf_labels * tf.log(y_conv), reduction_indices=[1]))
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(tf_labels,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
tf.scalar_summary(cross_entropy.op.name, cross_entropy)
tf.scalar_summary('training_acc', accuracy)
# --------------------------------------------------
# optimization
optimizer = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

# visualization
#grab only the first element of the batch and 16 filters
layer1_image1 = h_conv1[0:1, :, :, 0:16]
layer1_image1 = tf.transpose(layer1_image1, perm=[3,1,2,0])
layer_combine_1 = tf.concat(2, [layer1_image1])
list_lc1 = tf.split(0, 16, layer_combine_1)
layer_combine_1 = tf.concat(1, list_lc1)

def vis_layer(V,ix,iy,channels):
    V = tf.slice(V,(0,0,0,0),(1,-1,-1,-1))
    V = tf.reshape(V,(iy,ix,channels))
    cy=4
    cx=8
    ix += 2
    iy += 2
    V = tf.image.resize_image_with_crop_or_pad(V,ix,iy)
    V = tf.reshape(V,(iy,ix,cy,cx)) 
    V = tf.transpose(V,(2,0,3,1)) #cy,iy,cx,ix
    V = tf.reshape(V,(1,cy*iy,cx*ix,1))
    return V
    
V1 = vis_layer(h_conv1,28,28,32)
V2 = vis_layer(tf.transpose(W_conv1,(2,0,1,3)),5,5,32)
tf.image_summary('act_images_1',V1)
tf.image_summary('weight_images_1', V2)
# combine this summary with tensorboard (and you get a decent output);
summary_op = tf.merge_all_summaries()
summary_dir = '/home/osboxes/CloudStation/ELEC677/hw2/cifarlogs'
summary_writer = tf.train.SummaryWriter(summary_dir, sess.graph)
#
sess.run(tf.initialize_all_variables())
batch_xs = np.zeros([batchsize,28,28,1])#setup as [batchsize, width, height, numberOfChannels] and use np.zeros()
batch_ys = np.zeros([batchsize,10])#setup as [batchsize, the how many classes]
step = 10000
for i in range(step): # try a small iteration size once it works then continue
    perm = np.arange(ntrain*nclass)
    np.random.shuffle(perm)
    for j in range(batchsize):
        batch_xs[j,:,:,:] = Train[perm[j],:,:,:]
        batch_ys[j,:] = LTrain[perm[j],:]
    if i%1000 == 0:
        #calculate train accuracy and print it
        train_accuracy = accuracy.eval(feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 1.0})
        print(train_accuracy)
#    if i==step-1:
        summary_str = sess.run(summary_op, feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 1.0})
        summary_writer.add_summary(summary_str, i)    
    optimizer.run(feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 0.5}) # dropout only during training

# --------------------------------------------------
# test

print("test accuracy %g"%accuracy.eval(feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0}))

sess.close()

